#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

[0.6.0] - 2023-07-15
====================

Added
-----

- Role configuration support for ``keycloak/users`` module. This changes the required
  input variable format.
- New module ``keycloak/oauth2-proxy-subclient`` module for configuring consumers of the
  OAuth2-Proxy framework and new client roles.

Changed
-------

- Changed the protocol mapper configuration on the OAuth2-Proxy client to fill a token
  claim with user roles, rather than groups. See `details on this workaround
  <https://github.com/oauth2-proxy/oauth2-proxy/issues/831#issuecomment-1636948356>`_.

[0.5.0] - 2023-07-15
====================

Added
-----

- Service account definitions for ``keycloak/client`` module

Changed
-------

- OAuth2-Proxy behavior is being changed. This is preparation for a more sensible user
  and group management framework. The ``keycloak/realm`` module no longer outputs
  customized annotations based on a list of input groups, but instead emits the
  endpoints the proxy exposes, and expects consumers to handle configuring their ingress
  annotations independently.

Removed
-------

- Group definitions are no longer part of ``keycloak/client`` ``roles``.

Moved
-----

- User and group creation is now in the new ``keycloak/users`` module

Fixed
-----

- Extracted variables from ``keycloak/realm`` files to dedicated ``variables.tf``
- Set missing required provider versions

[0.4.0] - 2023-05-06
====================

Added
-----

* Outputs from ``client`` for configuring client applications

[0.3.0] - 2023-05-06
====================

Added
-----

* ``client`` module for configuring new OIDC clients

Changed
-------

* Require Terraform ``>= 1.4.6``
* Unified ``versions.tf`` for Keycloak modules

[0.2.0] - 2023-05-02
====================

Changed
-------

* Modules to new paths:
  * ``infrastructure`` -> ``keycloak/application``
  * ``realm`` -> ``keycloak/realm``

[0.1.0] - 2023-05-02
====================

Added
-----

* Initial modules migrated from BauerNet
