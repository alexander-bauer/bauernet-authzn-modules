Authentication
==============

This component configures `Keycloak <https://www.keycloak.org/>`_ for OIDC
authentication across the infrastructure.

Other components may use the outputs provided by this component to set up a
Terraform Keycloak provider, and configure their own Keycloak clients.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: keycloak-infrastructure.tf
   :caption: ``keycloak-infrastructure.tf``

.. literalinclude:: keycloak-output.tf
   :caption: ``keycloak-output.tf``
