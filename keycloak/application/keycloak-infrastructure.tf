variable "domain" {
  type = string
}
variable "certificate_issuer" {
  type = string
}

locals {
  keycloak_fqdns = ["keycloak.${var.domain}"] # only one allowed

  keycloak_admin_user = "admin"
}

resource "kubernetes_namespace" "auth" {
  metadata {
    name = "auth"
  }
}

resource "random_password" "keycloak_admin" {
  length  = 32
  special = true
}

resource "helm_release" "keycloak" {
  name       = "keycloak"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "keycloak"
  namespace  = one(kubernetes_namespace.auth.metadata).name

  values = [
    yamlencode({
      service = {
        type = "ClusterIP"
      }
      ingress = {
        enabled  = true
        hostname = one(local.keycloak_fqdns)
        annotations = {
          "cert-manager.io/cluster-issuer" = var.certificate_issuer
        }
        tls = true
      }
      auth = {
        adminUser     = local.keycloak_admin_user
        adminPassword = random_password.keycloak_admin.result
      }
    }),
  ]
}
