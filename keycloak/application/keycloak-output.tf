locals {
  keycloak_parameters = {
    client_id     = "admin-cli"
    url           = "https://${one(local.keycloak_fqdns)}"
    initial_login = false
    base_path     = "" # we are on the new Quarkus distribution
  }
  keycloak_credentials = {
    username = local.keycloak_admin_user
    password = random_password.keycloak_admin.result
  }
}

output "keycloak_namespace" {
  value = one(kubernetes_namespace.auth.metadata).name
}

output "keycloak_domain" {
  value = one(local.keycloak_fqdns)
}

output "keycloak_parameters" {
  value = local.keycloak_parameters
}

output "keycloak_credentials" {
  value     = local.keycloak_credentials
  sensitive = true
}
