variable "realm" {
  description = "Realm name to create the client in"
  type        = string
}

variable "client_id" {
  description = "ID slug for the client"
  type        = string
}

variable "client_name" {
  description = "Display name for the client"
  type        = string
}

variable "access_type" {
  description = "OpenID Access Type"
  type        = string
  default     = "CONFIDENTIAL"
}

variable "redirect_uris" {
  description = "Allowed redirect URIs"
  type        = list(string)
}

variable "roles" {
  description = "Keycloak roles to create"
  type = map(object({
    description = string
    users       = optional(list(string))
  }))
  default = {}
}
