output "id" {
  description = "UUID for client in Keycloak"
  value       = keycloak_openid_client.client.id
}

output "client_id" {
  description = "String representing client in Keycloak"
  value       = keycloak_openid_client.client.client_id
}

output "secret" {
  value     = keycloak_openid_client.client.client_secret
  sensitive = true
}

output "token_roles" {
  value = "resource_access.${keycloak_openid_client.client.client_id}.roles"
}
