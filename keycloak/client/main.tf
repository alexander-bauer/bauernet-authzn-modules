data "keycloak_realm" "realm" {
  realm = var.realm
}

resource "keycloak_openid_client" "client" {
  realm_id    = data.keycloak_realm.realm.id
  client_id   = var.client_id
  name        = var.client_name
  enabled     = true
  description = "OIDC Client for ${var.client_name}"

  access_type           = var.access_type
  valid_redirect_uris   = var.redirect_uris
  standard_flow_enabled = true
}

resource "keycloak_role" "roles" {
  for_each    = var.roles
  realm_id    = data.keycloak_realm.realm.id
  client_id   = keycloak_openid_client.client.id
  name        = each.key
  description = each.value.description
}

locals {
  # Assemble a set of group names referenced across any role.
  groups = toset(concat([for name, role in var.roles : role.groups]...))
}

data "keycloak_group" "groups" {
  for_each = local.groups
  realm_id = data.keycloak_realm.realm.id
  name     = each.value
}

resource "keycloak_group_roles" "group_roles" {
  for_each = data.keycloak_group.groups
  realm_id = data.keycloak_realm.realm.id
  group_id = each.value.id
  role_ids = [
    for name, role in var.roles : keycloak_role.roles[name].id
    if contains(role.groups, each.key)
  ]
}

locals {
  # Assemble a set of user names referenced across any role.
  users = toset(concat([for name, role in var.roles : role.users != null ? role.users : []]...))
}

data "keycloak_user" "users" {
  for_each = local.users
  realm_id = data.keycloak_realm.realm.id
  username = keycloak_openid_client.client.id
}

resource "keycloak_user_roles" "user_roles" {
  for_each = data.keycloak_user.users
  realm_id = data.keycloak_realm.realm.id
  user_id  = each.value.id
  role_ids = [
    for name, role in var.roles : keycloak_role.roles[name].id
    if((role.users != null) && contains(role.users, each.key))
  ]
}
