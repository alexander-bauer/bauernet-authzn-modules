resource "keycloak_realm" "primary" {
  realm             = var.primary_realm.id
  enabled           = true
  display_name      = var.primary_realm.name
  display_name_html = "<b>${var.primary_realm.name}</b>"

  login_theme = "keycloak"

  ssl_required    = "all"
  password_policy = "length(16) and notUsername"

  user_managed_access      = true
  reset_password_allowed   = true
  verify_email             = true
  login_with_email_allowed = true

  smtp_server {
    host = var.smtp.host
    from = replace(var.smtp.from, "/%s/", "keycloak")

    #auth {
    #  username = local.email.smtp.username
    #  password = local.email.smtp.password
    #}
  }

  internationalization {
    supported_locales = ["en"]
    default_locale    = "en"
  }

  security_defenses {
    headers {
      x_frame_options                     = "DENY"
      content_security_policy             = "frame-src 'self'; frame-ancestors 'self'; object-src 'none';"
      content_security_policy_report_only = ""
      x_content_type_options              = "nosniff"
      x_robots_tag                        = "none"
      x_xss_protection                    = "1; mode=block"
      strict_transport_security           = "max-age=31536000; includeSubDomains"
    }
    brute_force_detection {
      permanent_lockout                = false
      max_login_failures               = 30
      wait_increment_seconds           = 60
      quick_login_check_milli_seconds  = 1000
      minimum_quick_login_wait_seconds = 60
      max_failure_wait_seconds         = 900
      failure_reset_time_seconds       = 43200
    }
  }

  web_authn_policy {
    relying_party_entity_name = var.primary_realm.name
    relying_party_id          = var.keycloak_domain
    signature_algorithms      = ["ES256", "RS256"]
  }
}

locals {
  realm_base_url = "${var.keycloak_base_url}/realms/${var.primary_realm.id}"
  realm_urls = {
    base                 = local.realm_base_url
    selfservice          = "${local.realm_base_url}/account"
    oidendpoint          = "${local.realm_base_url}/realms/${var.primary_realm.id}"
    oidc_auth            = "${local.realm_base_url}/protocol/openid-connect/auth"
    oidc_token           = "${local.realm_base_url}/protocol/openid-connect/token"
    oidc_userinfo        = "${local.realm_base_url}/protocol/openid-connect/userinfo"
    oidc_logout          = "${local.realm_base_url}/protocol/openid-connect/logout"
    oidc_logout_redirect = "${local.realm_base_url}/protocol/openid-connect/logout?redirect_uri=%s"
  }
}

output "realm" {
  value = keycloak_realm.primary
}

output "realm_urls" {
  value = local.realm_urls
}
