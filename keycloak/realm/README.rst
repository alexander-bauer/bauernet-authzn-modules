Realm
=====

This component is responsible for configuring a particular authentication
realm, which is underneath the Keycloak ``master`` realm. This means this
component is also responsible for provisioning user accounts and group
management.

See `Keycloak Best Practices
<https://www.keycloak.org/docs/latest/server_admin/index.html#configuring-realms>`_.

OAuth2 Proxy
------------

Many applications today support OIDC natively, and so are able to be configured
to use Keycloak for |SSO|. However, some applications either do not support
OIDC, or elide authentication altogether, leaving the work to an authenticating
proxy. To serve this role in this architecture, we use `OAuth2-Proxy
<https://github.com/oauth2-proxy/oauth2-proxy>`_.

.. todo:: Clean up this section.

NGINX Ingress supports querying a separate endpoint for authorization ahead of
permitting requests to any ingress. OAuth2-Proxy is arranged to be used for
this purpose.

When a user attempts to reach a protected ingress, NGINX makes a request to
OAuth2-Proxy's ``auth`` endpoint, which returns either ``200 OK`` or ``401
UNAUTHORIZED``. In the latter case, NGINX renders a redirect to the requestor,
sending them to the OAuth2-Proxy sign-in endpoint, which initiates the login
flow with Keycloak. Keycloak has the user sign in, hands them their
authorization tokens, and redirects them to OAuth2-Proxy. OAuth2-Proxy
validates the tokens (including checking the desired group membership for the
specific protected application), hands the user another cookie [#]_ containing
a session reference, and finally redirects them to the protected application
endpoint. When NGINX receives the request for that endpoint, it makes another
request to the ``auth`` endpoint, this time receiving ``200 OK``, and then
passes the original request on to the application.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Terraform Configuration
-----------------------

.. literalinclude:: keycloak-infrastructure.tf
   :caption: ``keycloak-infrastructure.tf``

.. literalinclude:: keycloak-output.tf
   :caption: ``keycloak-output.tf``

.. rubric:: Footnotes

.. [#] The cookie must be domain-scoped (to the base domain) so that it can be
   set by OAuth2-Proxy (on one subdomain) and read by NGINX at the protected
   application endpoint (on a different subdomain).
