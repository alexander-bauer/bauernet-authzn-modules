locals {
  oauth2_proxy_fqdn = "authproxy.${var.domain}"
  oauth2_proxy_url  = "https://${local.oauth2_proxy_fqdn}"
  oauth2_proxy_client = {
    id   = "oauth2-proxy"
    name = "oauth2-proxy"
  }
}

resource "keycloak_openid_client" "oauth2_proxy" {
  realm_id              = keycloak_realm.primary.id
  client_id             = local.oauth2_proxy_client.id
  name                  = local.oauth2_proxy_client.name
  enabled               = true
  description           = "oauth2-proxy client"
  access_type           = "CONFIDENTIAL"
  standard_flow_enabled = true
  valid_redirect_uris   = ["${local.oauth2_proxy_url}/oauth2/callback"]
  base_url              = local.oauth2_proxy_url
}

# This mapper attaches the whole list of group membership to the token.
#resource "keycloak_openid_group_membership_protocol_mapper" "group_membership_mapper" {
#  realm_id  = keycloak_realm.primary.id
#  client_id = keycloak_openid_client.oauth2_proxy.id
#  name      = "group-membership-mapper"
#
#  claim_name = "groups"
#}

resource "keycloak_openid_user_client_role_protocol_mapper" "role_membership_mapper" {
  realm_id  = keycloak_realm.primary.id
  client_id = keycloak_openid_client.oauth2_proxy.id
  name      = "role-membership-mapper"

  claim_name = "groups"
}

# https://github.com/oauth2-proxy/oauth2-proxy/issues/1913
resource "keycloak_openid_audience_protocol_mapper" "audience_mapper" {
  realm_id                 = keycloak_realm.primary.id
  client_id                = keycloak_openid_client.oauth2_proxy.id
  name                     = "audience-mapper"
  included_client_audience = keycloak_openid_client.oauth2_proxy.name
}

resource "random_password" "oauth2_proxy_cookie_secret" {
  length           = 32
  override_special = "-_"
}

resource "helm_release" "oauth2_proxy" {
  name       = "oauth2-proxy"
  repository = "https://oauth2-proxy.github.io/manifests"
  chart      = "oauth2-proxy"
  namespace  = one(data.kubernetes_namespace.keycloak.metadata).name
  version    = "6.7.0"

  values = [
    yamlencode({
      config = {
        clientID     = keycloak_openid_client.oauth2_proxy.client_id
        clientSecret = keycloak_openid_client.oauth2_proxy.client_secret
        cookieSecret = random_password.oauth2_proxy_cookie_secret.result
      }
      extraArgs = {
        provider              = "keycloak-oidc"
        provider-display-name = keycloak_realm.primary.display_name
        oidc-issuer-url       = local.realm_base_url
        keycloak-group        = "account"         # base group; application groups are supplied with requests
        code-challenge-method = "S256"            # PKCE exchange method
        skip-provider-button  = true              # go straight to auto-login
        whitelist-domain      = "*.${var.domain}" # allowed domains after auth redirect
        cookie-domain         = ".${var.domain}"  # cookie must be domain-scoped, see readme
        set-xauthrequest      = true              # set user headers in response to /auth
        silence-ping-logging  = true
        provider-ca-file      = "/etc/trust-bundle.pem"
      }
      ingress = {
        enabled = true
        annotations = {
          "cert-manager.io/cluster-issuer" = var.certificate_issuer
        }
        hosts = [local.oauth2_proxy_fqdn]
        tls = [{
          secretName = "oauth2-proxy-tls"
          hosts      = [local.oauth2_proxy_fqdn]
        }]
      }
      sessionStorage = {
        type = "redis" # rather than cookie
      }
      redis = {
        enabled      = true
        architecture = "standalone" # rather than replicated
      }
      extraVolumes = [{
        name = "trust-bundle"
        configMap = {
          name = var.trust_bundle
        }
      }]
      extraVolumeMounts = [{
        name      = "trust-bundle"
        mountPath = "/etc/trust-bundle.pem"
        subPath   = "bundle.pem"
      }]
    }),
  ]
}

locals {
  oauth2_urls = {
    base   = local.oauth2_proxy_url
    auth   = "${local.oauth2_proxy_url}/oauth2/auth"
    start  = "${local.oauth2_proxy_url}/oauth2/start"
    logout = "${local.oauth2_proxy_url}/oauth2/sign_out"
  }
  oauth2_headers = {
    user  = "X-Auth-Request-Preferred-Username" # from set-xauthrequest
    email = "X-Auth-Request-Email"              # from set-xauthrequest
  }
}

output "auth_proxy" {
  value = {
    base_url = local.oauth2_proxy_url
    headers  = local.oauth2_headers
    urls     = local.oauth2_urls
  }
}
