variable "primary_realm" {
  type = object({
    id   = string
    name = string
  })
}

variable "keycloak_domain" {
  type = string
}

variable "keycloak_base_url" {
  type = string
}

variable "smtp" {
  type = object({
    host = string
    from = string
  })

  validation {
    condition     = length(regexall("%s", var.smtp.from)) == 1
    error_message = "smtp from must contain a one template substitution ('%s')"
  }
}

variable "domain" {
  type = string
}

variable "keycloak_namespace" {
  type = string
}

variable "certificate_issuer" {
  type = string
}

variable "trust_bundle" {
  type = string
}

data "kubernetes_namespace" "keycloak" {
  metadata {
    name = var.keycloak_namespace
  }
}
