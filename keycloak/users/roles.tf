locals {
  # A flattened set of role names.
  all_roles = toset(flatten([for g in var.groups : g.roles]))

  # Reformat from client:role to objects for easier parsing
  role_objects = {
    for r in local.all_roles : r => {
      client = split(":", r)[0]
      role   = split(":", r)[1]
    }
  }
}

data "keycloak_openid_client" "clients" {
  # For every mentioned client
  for_each  = toset([for r in local.role_objects : r.client])
  realm_id  = var.realm_id
  client_id = each.value
}

data "keycloak_role" "roles" {
  # All roles, indexed as "client:role"
  for_each  = local.role_objects
  realm_id  = var.realm_id
  client_id = data.keycloak_openid_client.clients[each.value.client].id
  name      = each.value.role
}
