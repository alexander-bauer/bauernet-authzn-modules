locals {
  groups_by_name    = { for g in var.groups : g.name => g }
  users_by_username = { for u in var.users : u.username => u }
}

resource "keycloak_group" "groups" {
  for_each = local.groups_by_name
  realm_id = var.realm_id
  name     = each.key
}

resource "keycloak_group_roles" "roles" {
  for_each = local.groups_by_name
  realm_id = var.realm_id
  group_id = keycloak_group.groups[each.key].id
  role_ids = [
    for r in each.value.roles :
    data.keycloak_role.roles[r].id # indexed by client:role
  ]
  exhaustive = true
}

resource "keycloak_user" "users" {
  for_each = local.users_by_username
  realm_id = var.realm_id

  username = each.value.username
  enabled  = each.value.enabled

  first_name = each.value.first
  last_name  = each.value.last

  email          = each.value.email
  email_verified = true # the initial email was validated, let's say
  #
  #  initial_password {
  #    value     = random_password.initial_passwords[each.key].result
  #    temporary = true
  #  }
}

resource "keycloak_user_groups" "users" {
  for_each = local.users_by_username

  realm_id = var.realm_id
  user_id  = keycloak_user.users[each.key].id

  exhaustive = true
  group_ids = [
    for group in each.value.groups :
    keycloak_group.groups[group].id
  ]
}
