variable "realm_id" {
  description = "Keycloak realm ID"
  type        = string
}

variable "groups" {
  description = "Collection of groups to create, and which roles (client:role format) to associate for each"
  type = set(object({
    name  = string
    roles = optional(list(string), []) # client:role
  }))
  validation {
    condition = alltrue([ # iterate through every role in every group
      for r in flatten([for g in var.groups : g.roles]) :
      can(regex("^[A-Za-z0-9_-]+:[A-Za-z0-9_-]+$", r))
    ])
    error_message = "Every role must be in \"<client>:<role_name>\" format"
  }
}

variable "users" {
  description = "Collection of users to create, and groups to assign to each"
  type = set(object({
    username = string
    first    = string
    last     = string
    email    = string
    enabled  = optional(bool, true)
    groups   = optional(list(string), [])
  }))
}
