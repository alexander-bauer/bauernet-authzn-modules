variable "realm" {
  description = "Realm name to operate in"
  type        = string
}

variable "client_id" {
  description = "ID of the already-existing OAuth2-Proxy client"
  type        = string
  default     = "oauth2-proxy" # same as from ../realm module
}

variable "base_url" {
  description = "Base URL for OAuth2-Proxy"
  type        = string
}

variable "roles" {
  description = "Roles to create for this subclient"
  type = map(object({
    description = optional(string, null)
  }))
}
