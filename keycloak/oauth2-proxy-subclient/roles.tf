resource "keycloak_role" "roles" {
  for_each = var.roles

  realm_id  = data.keycloak_realm.realm.id
  client_id = data.keycloak_openid_client.oauth2-proxy.id

  name        = each.key
  description = each.value.description
}

locals {
  auth_url_by_role = {
    for r in keys(var.roles) : r => "${local.auth_url}?allowed_groups=${urlencode("role:${var.client_id}:${r}")}"
  }
}
