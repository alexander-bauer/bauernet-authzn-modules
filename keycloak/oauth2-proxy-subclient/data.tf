data "keycloak_realm" "realm" {
  realm = var.realm
}

data "keycloak_openid_client" "oauth2-proxy" {
  realm_id  = data.keycloak_realm.realm.id
  client_id = var.client_id
}
