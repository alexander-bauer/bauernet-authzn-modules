locals {
  auth_url   = "${var.base_url}/oauth2/auth"
  signin_url = "${var.base_url}/oauth2/start"
}
