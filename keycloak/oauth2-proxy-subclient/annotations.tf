output "annotations" {
  description = "NGINX Ingress Controller annotations by role for using OAuth2-Proxy"
  value = {
    nginx = {
      role = {
        for r in keys(var.roles) : r => {
          "nginx.ingress.kubernetes.io/auth-url"    = local.auth_url_by_role[r]
          "nginx.ingress.kubernetes.io/auth-signin" = local.signin_url
        }
      }
    }
  }
}
